# Hardware

![](Bando.jpg)

Bass side before modification

![](parts.jpg)

Some of the parts used

![](bass1.jpg)

The bass side: View of the complete wiring

![](bass2.jpg)

The bass side: Pressure sensor

![](bass3.jpg)

The bass side: ESP32 and 2xMCP23017

![](power.jpg)

The bass side: Power supply. LiPo-battery and charge/discharge board. On the left the on/off-switch, on the right the power jack.

![](discant1.jpg)

The discant side: View of the complete wiring

![](discant2.jpg)

The discant side: 2xMCP23017

![](discant3.jpg)

The discant side: Valves removed, micro switch actuators visible
