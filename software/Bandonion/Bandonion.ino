/*
 * Electronical Bandoneon, sending data via AppleMIDI (RTPMidi)
 *
 * This program is only written for ESP32 based developer boards. For other MCU, other libraries
 * and/or lots of code changes are necessary.
 *
 * The controller first tries to run as WiFi station (SSID and password see later); if it cannot
 * connect to the network, it starts as WiFi AP with the given SSID and password.
 *
 * To minimize latencies, there should be a direct WiFi connection between the bandoneon and
 * the MIDI sequencer. Routers and APs in between may increase latency.
 *
 * Libraries needed:
 *
 * For wireless communication:
 *    WiFi.h
 *    WiFiClient.h
 *    ESPmDNS
 *
 * For OTA-Updates and configuration:
 *    SPIFFS.h
 *    WebServer.h
 *    Update.h
 *    Preferences.h
 *
 * For querying buttons and pressure sensor:
 *    Wire.h
 *    Adafruit_MCP23017.h
 *    sdpsensor.h
 *
 * For AppleMiDi:
 *    AppleMiDi.h
 *
 * The project is compiled and uploaded using Arduino IDE V1.8.9
 * All libraries are included from ESP32 core V1.04 (Espressif Systems), except AppleMiDi.h V2.1.0 (lathoub@gmail.com), Adafruit_MCP23017.h V1.04 (info@adafruit.com) and sdpsensor.h V0.0.6 (jwi@sensirion.com)
 * As the webserver is only used for configuration and OTA, we do not use the asyncWebserver.
 *
 * Files in SPIFFS:
 *
 *    /notes:       Matching of buttons to MIDI pitches
 *    /index.html:  Website index file
 *    /config.html: Configuration file
 *    /style.css:   Website style file
 *
 * LICENSE:
 *
 * Except from some libraries used, the code is free. USE AT YOUR OWN RISK.
 */

/*
 * If TEST is defined, the pressure sensor is not used and the loop has a delay
 */

//#define TEST

// For connection via WiFi, mDNS, Webserver, SPIFFS, OTA and AppleMIDI

#include <SPIFFS.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <detail/RequestHandlersImpl.h>   // only for detecting file types
#include <ESPmDNS.h>
#include <Update.h>
#include <Preferences.h>
#include <AppleMIDI.h>

WebServer server(80);

// Sensors via I²C: MCP23017 (Buttons) and SDP811 (pressure)

#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <sdpsensor.h>

#define mcpAddress 0x20   // Base addresse of MCP23017
#define sdpAddress 0x26   // Addresse of the pressure sensor (SDP811)

// Definitions and objects

#define NUM_BUTTONS 64      // For up to 64 buttons (we need 55, leave some alone)
#define NUM_BASS 32         // Button number this less means bass side
Adafruit_MCP23017 mcp[4];   // We need four MCP23017 (= 64 Ports)
SDPSensor sdp(sdpAddress);  // The pressure sensor

bool button[NUM_BUTTONS];   // The actual state of the buttons (TRUE = pressed)
bool button_m[NUM_BUTTONS]; // The former state of the buttons
uint8_t volume_m = 0;       // The former value of the volume (speed)
int direction = 0;          // The actual direction
int direction_m = 0;        // The former direction
int pitchbend_m = 0;        // The former pitch bend value
bool portExists[4]={0,0,0,0};   // Do we have all the MCP23017??

/*
 * These are the defaults, and could be overwritten using preferences in SPIFFS
 * or the /config web page
 */

#define DEFAULT_SSID "aMidiAP"
#define DEFAULT_PASS "Bandonion"

Preferences preferences;
uint8_t note[NUM_BUTTONS][2];   // Mapping of the buttons/MIDI pitches
const char* durbase[] = { "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#" };

uint8_t velocity = 127;         // Base volume of the pitches
bool    octave   = false;       // Octave chorus: yes/no
uint8_t channelb = 1;           // MIDI channel used for bass
uint8_t channeld = 1;           // MIDI channel used for discant
uint8_t progb    = 23;          // MIDI program used for bass
uint8_t progd    = 23;          // MIDI program used for discant
uint8_t transp   = 0;           // #Halftones to transpose from A
char ssid[32] = DEFAULT_SSID;   // SSID of the AP to connect to
char pass[32] = DEFAULT_PASS;   // Password of the AP

// Create AppleMIDI instance named "bandonion" on port 5004
// This has changed from an earlier version of the AppleMIDI library

APPLEMIDI_CREATE_INSTANCE(WiFiUDP, MIDI, "bandonion", 5004);

unsigned int t1 = millis();   // Only for test, to create loop delay
bool isConnected = false;     // Whether there is a MIDI connection
bool alloff = false;          // Are all notes off?

/*
 * This is just to signal some states via LEDs
 */

#define LEDy 17 //  yellow LED: blinking when looking for WLAN, steady when connected
#define LEDg 16 //  green LED; steady when AppleMiDi connected

/*
 * Setup: Initialize sensors, WiFi, and MIDI settings
 */

void setup() {
  Serial.begin(115200);
  pinMode(LEDy, OUTPUT); digitalWrite(LEDy, LOW);
  pinMode(LEDg, OUTPUT); digitalWrite(LEDg, LOW);
  bool yon = false;

  /*
   * Mount SPIFFS and read the pitch mapping file and configuration
   * Important: SPIFFS must be mounted _before_ defining the server handlers
   */

  SPIFFS.begin();

  File notes = SPIFFS.open("/notes", "r");

  // This could be done more intelligent. But it works this way.

  for (int i = 0; i < NUM_BUTTONS; i++) {
    if (notes.available()) {
      if (i == notes.parseInt()) {
        note[i][0] = notes.parseInt();
        note[i][1] = notes.parseInt();
      }
    }
  }
  notes.close();

  if (!getPreferences()) {
    Serial.println("Could not get the preferences; try to write defaults");
    if (!putPreferences()) {
      Serial.println(" -- this too did not work!");
    }
  }

  printPreferences();

  // Start WiFi connection. If no AP is found, create AP.

  int i = 10;
  Serial.print("Getting IP address ");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  while ((WiFi.status() != WL_CONNECTED) && (i-- > 0))  {
    delay(500);
    Serial.print(".");
    if (yon) digitalWrite(LEDy, LOW);
    else digitalWrite(LEDy, HIGH);
    yon = !yon;
  }
  if (i > 0) {
    Serial.println(" Done. ");
    Serial.print("My IP Address is ");
    Serial.println(WiFi.localIP());
  }
  else {
    WiFi.mode(WIFI_AP);
    Serial.println(" No connection. Starting AP.");
    WiFi.softAP(ssid, pass);
    Serial.print("My IP Addres is ");
    Serial.println(WiFi.softAPIP());
  }

  digitalWrite(LEDy, HIGH);

  // We use mDNS to announce as "bandonion.local"

  if (!MDNS.begin("bandonion")) {
     Serial.println("Error setting up MDNS responder!");
     delay(1000);
  }


  /*
   * Webserver for settings and OTA update
   */

  server.serveStatic("/", SPIFFS, "/index.html");
  server.on("/list", HTTP_GET, handleList);
  server.on("/status", HTTP_GET, handleStatus);
  server.on("/upload", HTTP_POST, []() {}, handleUpload);
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    delay(1000);
    ESP.restart();
    }, handleUpdate);
  server.on("/reboot", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", "--- Bandonion rebooting ---");
    delay(1000);
    ESP.restart();
    });
  server.on("/config", HTTP_GET, handleConfig);
  server.on("/transp", HTTP_GET, handleTransp);
  server.on("/config", HTTP_POST, changeConfig);
  server.on("/transp", HTTP_POST, changeTransp);
  server.onNotFound(handleUnknown);
  server.begin();
  MDNS.addService("http", "tcp", 80);

  // Initialise I²C

  Wire.begin();
  Wire.setClock(400000);    // If having TWI problems, reduce to 100000

  // Create the mcp objects and set the ports to INPUT_PULLUP

  Serial.println("Looking for extension ports:");
  for (int i=0;i<4;i++) {
    if (findPorts(i)) {
      portExists[i] = true;
      mcp[i].begin(i);
      for (int j = 0; j < 16; j++) {
        mcp[i].pinMode(j, INPUT);
        mcp[i].pullUp(j, HIGH);  // turn on a 100K pullup internally
      }
    }
  }

  // Initialize pressure sensor. This may take some time.

#ifndef TEST
  do {
    int ret = sdp.init(false);
    if (ret == 0) {
      Serial.print("init(): success\n");
      break;
    } else {
      Serial.print("init(): failed, ret = ");
      Serial.println(ret);
      delay(1000);
    }
  } while(true);
#endif

  // Wait for MIDI connection, then send the basic MIDI settings

  MIDI.begin();

  MDNS.addService("apple-midi", "udp", 5004);

  AppleMIDI.setHandleConnected(OnAppleMidiConnected);
  AppleMIDI.setHandleDisconnected(OnAppleMidiDisconnected);
  AppleMIDI.setHandleError(OnAppleMidiError);
  MIDI.setHandleControlChange(OnMidiControlChange);
  Serial.print("Waiting for MIDI connection ... ");
  while (!isConnected) {
    server.handleClient();    // Although waiting for MIDI, we should handle web services
    MIDI.read();
    delay(100);
  }

  Serial.println("\nDone.");
  setProgram(progb, channelb);
  if (channelb != channeld) setProgram(progd, channeld);

  // Set the initial state of the buttons

  for (int i = 0; i < NUM_BUTTONS; i++) button_m[i] = false;

  // Switch the LEDs from yellow to green

  digitalWrite(LEDy, LOW);
  digitalWrite(LEDg, HIGH);

  Serial.println(" ---- starting loop ----");
}

/*
 * This is where all the work is done
 */

void loop() {

  // Look for web and MIDI requests

  server.handleClient();
  MIDI.read();

  // Only act as long as the MIDI connection is active
  // (well, in fact if it is not the controller reboots)

#ifdef TEST
  if (isConnected && ((millis() - t1) > 500)) {
    t1 = millis();
#else
  if (isConnected) {
#endif

    // First, read the pressure to set volume and direction.
    // Raw pressure values are 16bit signed integers.

    int pressure;
#ifdef TEST
    pressure = 20000;
#else
    int ret = sdp.readSample();
    if (ret == 0) pressure = sdp.getRawPressure();
#endif

    // there is some tolerance around zero, so set a band of +- 100

    if (pressure > 100) direction = 0;
    else if (pressure < -100) {
      direction = 1;
      pressure = -pressure;
    }
    else pressure = 0;

    uint8_t volume = presToVol(pressure);

    /*
     * We now have pressure, volume, and direction, so send MIDI pitches and controls.
     * First, send possibly running status messages (note on/off)
     */

    getSensors();

    // On direction change, (almost) all piches will change, so first send "All notes off",
    // then new "Note on" for all buttons pressed

    if (direction != direction_m) {
      allNotesOff();
      for (int i = 0; i < NUM_BUTTONS; i++) {
        if (button[i])
          if (i < NUM_BASS) mNoteOn(note[i][direction]+transp, channelb);
          else mNoteOn(note[i][direction]+transp, channeld);
      }
      direction_m = direction;
    }

    // Otherwise, look for newly pressed or released buttons and send "Note on" or "Note off", rexpectively.
    // Only send MIDI notes if something changed

    else {
      bool buttonPressed = false;
      for (int i = 0; i < NUM_BUTTONS; i++) {
        uint8_t channel = (i < NUM_BASS? channelb : channeld);
        if (button_m[i] && !button[i]) mNoteOff(note[i][direction]+transp, channel);
        if (button[i]) {
#ifdef TEST
          Serial.print("Button: ");   // Need this mainly to create the mapping file
          Serial.println(i);
#endif
          buttonPressed = true;
          alloff = false;
          if (!button_m[i]) mNoteOn(note[i][direction]+transp, channel);
        }
        button_m[i] = button[i];
      }

      // If no button is pressed, and there were some pressed before, send "All notes off"

      if (!buttonPressed)
        if (!alloff) {
          allNotesOff();
          alloff = true;
        }
    }

    // Only send volume/bend information if buttons are pressed

    //if (!alloff) {

      // Send volume info, if volume changed

      if (volume != volume_m) {
        volume_m = volume;
        mSetVolume(volume);
      }

      // Send pitch bend, if negative or changed

      int pitchbend = presToBend(pressure);
      if (pitchbend != pitchbend_m) {
        if (pitchbend < 0) mPitchBend(pitchbend);
        else if (pitchbend_m < 0) mPitchBend(0);
        pitchbend_m = pitchbend;
      }
    //}
  }
}

/*
 * Functions to get, set and print preferences (see also the web-handlers later)
 */

bool getPreferences() {
  if (preferences.begin("config", true)) { // open preferences for read-only
    velocity = preferences.getChar("velocity", velocity);
    octave   = preferences.getBool("octave", octave);
    channelb = preferences.getChar("channelb", channelb);
    channeld = preferences.getChar("channeld", channeld);
    progb    = preferences.getChar("progb", progb);
    progd    = preferences.getChar("progd", progd);
    transp   = preferences.getChar("transp", transp);
    if (!preferences.getString("ssid", ssid, 32)) strcpy(ssid, DEFAULT_SSID);
    if (!preferences.getString("pass", pass, 32)) strcpy(pass, DEFAULT_PASS);
    preferences.end();
    return true;
  }
  else return false;
}

bool putPreferences() {
  if (preferences.begin("config", false)) { // open preferences for write
    preferences.clear();
    preferences.putChar("velocity", velocity);
    preferences.putBool("octave", octave);
    preferences.putChar("channelb", channelb);
    preferences.putChar("channeld", channeld);
    preferences.putChar("progb", progb);
    preferences.putChar("progd", progd);
    preferences.putChar("transp", transp);
    preferences.putString("ssid", ssid);
    preferences.putString("pass", pass);
    preferences.end();
    return true;
  }
  else return false;
}

void printPreferences() {
    Serial.println("Current preferences: ");
    Serial.print("velocity "); Serial.println(velocity);
    Serial.print("octave "); Serial.println(octave);
    Serial.print("channelb "); Serial.println(channelb);
    Serial.print("channeld "); Serial.println(channeld);
    Serial.print("progb "); Serial.println(progb);
    Serial.print("progd "); Serial.println(progd);
    Serial.print("transp "); Serial.println(transp);
    Serial.print("ssid "); Serial.println(ssid);
    Serial.print("pass "); Serial.println(pass);
    Serial.println();
}

/*
 * Basic functions for the Bandonion to play
 */

// Is MCP23017 #port available?

bool findPorts(int port) {
  int err;
  Serial.print("Port ");
  Serial.print(port);
  Wire.beginTransmission(mcpAddress + port);
  Wire.write((byte)0x0a); // IOCON
  err =  Wire.endTransmission();
  if (err==0 ) {
    Serial.println(" exists");
    return true;
  }
  Serial.println(" missing!");
  return false;
}

// Which buttons are pressed?

bool getSensors(void) {
  bool ret = false;
  for (int c = 0; c < 4; c++) {
    if (portExists[c]) {
      uint16_t val = mcp[c].readGPIOAB();
      for (int p = 0; p < 16; p++) {
        int i = 16*c + p;
        if (i < NUM_BUTTONS) {
          button[i] = false;
          if (!(val & 0x0001)) {
            button[i] = true;
            ret = true;
          }
          val = val >> 1;
        }
      }
    }
  }
  return ret;
}

/*
 * Functions to calculate volume and pitch bend from the pressure.
 * First approach was to have a non-linear volume up to a (raw) pressure of 24000,
 * then induce a pitch bend. This didn't sound very well, so now the volume is linear up to 32000,
 * and there is no pitch bend. Could be improved when a better calculation is found.
 */

// This is an experimental function to calculate the volume from the pressure

uint8_t presToVol(int pressure) {
  float vol;
//  float base = float(pressure)/24000.0;
  float base = float(pressure)/32000.0;
  if (base > 1.0) vol = 1.0;
//  else vol = sqrt(base);
  else vol = base;
  uint8_t ret = int(127.0*vol);
  return ret;
}

// This is an experimental function to calculate the pitch bend from the pressure

int presToBend(int pressure) {
//      return (24000-pressure)/8;
  return 0;
}

/*
 * Send MIDI commands. See the documents at midi.org.
 */

void mNoteOn(uint8_t note, uint8_t channel) {
//  Serial.print("Note on ");
//  Serial.println(note);
  MIDI.sendNoteOn(note, velocity, channel);
  if (octave) MIDI.sendNoteOn(note-12, velocity, channel);
}

void mNoteOff(uint8_t note, uint8_t channel) {
//  Serial.print("Note off ");
//  Serial.println(note);
  MIDI.sendNoteOff(note, 0, channel);
  if (octave) MIDI.sendNoteOff(note-12, 0, channel);
}

void allNotesOff() {
//  Serial.println("All Notes off");
  MIDI.sendControlChange(123, 0, channelb);
  if (channelb != channeld)
    MIDI.sendControlChange(123, 0, channeld);
}

void mSetVolume(uint8_t vol) {
//  Serial.print("Set volume ");
//  Serial.println(vol);
  MIDI.sendControlChange(7, vol, channelb);
  if (channelb != channeld)
    MIDI.sendControlChange(7, vol, channeld);
}

void mPitchBend(int pitch) {
//  Serial.print("PitchBend ");
//  Serial.println(pitch);
  MIDI.sendPitchBend(pitch, channelb);
  if (channelb != channeld)
    MIDI.sendPitchBend(pitch, channeld);
}

void setProgram(uint8_t prg, uint8_t channel) {
//  Serial.print("Set Program ");
//  Serial.println(prg);

  MIDI.sendControlChange(7, 0, channel);
  MIDI.sendProgramChange(prg, channel);
}

/*
 * Event handlers for incoming MIDI messages
 */

// rtpMIDI session. Device connected

void OnAppleMidiConnected(const uint32_t& ssrc, const char* name) {

  isConnected = true;
  Serial.print("Connected to session ");
  Serial.print(name);
  Serial.print(", ssrc = ");
  Serial.println(ssrc);
}

// rtpMIDI session. Device disconnected

void OnAppleMidiDisconnected(const uint32_t& ssrc) {
  isConnected = false;
  Serial.print("Disconnected ");Serial.println(ssrc);
  exit(0);  //  This forces a reboot!
}

// rtpMIDI session. Error occorded during processing

void OnAppleMidiError(const uint32_t& ssrc, int32_t errorCode) {
  Serial.print("ERROR: ");
  Serial.println(errorCode);
  exit(1);  // This forces a reboot!
}

// MIDI message. Control Change (CC)

void OnMidiControlChange(byte channel, byte number, byte value) {
  Serial.print("CONTROL CHANGE: ");
  Serial.print("channel: "); Serial.print(channel);
  Serial.print(" number: "); Serial.print(number);
  Serial.print(" value: "); Serial.println(value);

  // Ignore channel. Switch on number
  switch (number) {
    case 3:
      if (value < 12) {
        transp = value % 12;
      } else if (value < 16) {
        switch (value) {
          case 12:
            progb = 23;
            progd = 23;
            break;
          case 13:
            progb = 16;
            progd = 16;
            break;
          case 14:
            progb = 42;
            progd = 40;
            break;
          case 15:
            progb = 68;
            progd = 73;
            break;
        }
        setProgram(progb, channelb);
        if (channelb != channeld) setProgram(progd, channeld);
      }
      break;
    default:
      break;
  }
}

/*
 * Webserver handlers (mostly serve parameters, SPIFFS and OTA)
 */

// Send 404, if no other handler found

void notFound(String url) {
  Serial.println("--- file not found ---");
  String HTML = String("<html><head><title>404 Not Found</title></head><body>") +
                "<h1>404 Not Found</h1>" +
                "<p>The requested URL " + url +
                " was not found on this webserver.</p>" +
                "</body></html>";
  server.send(404, "text/html", HTML);
}

// Try to find requested file in SPIFFS

void handleUnknown() {
  String filename = server.uri();
  File pageFile = SPIFFS.open(filename, "r");
  if (pageFile) {
    String contentTyp = StaticRequestHandler::getContentType(filename);
    size_t sent = server.streamFile(pageFile, contentTyp);
    pageFile.close();
  }
  else
    notFound(filename);
}

// Very simple listing of SPIFFS content

void handleList() {
  File root = SPIFFS.open("/");
  String output = "Files in SPIFFS:\r\n";
  if(root.isDirectory()){
    File file = root.openNextFile();
    while(file){
      output += "Typ: ";
      output += (file.isDirectory()) ? "dir" : "file";
      output += "\tName: ";
      output += String(file.name());
      output += "\r\n";
      file = root.openNextFile();
    }
  }
  output += "\r\n";
  server.send(200, "text/plain", output);
}

// Very simple status control

void handleStatus() {
  String output = "Current status:\r\n";
  for (int i = 0; i < 4; i++) {
    output += "Port ";
    output += i;
    if (portExists[i]) output += " exists\r\n";
    else output += " is missing!\r\n";
  }
  if (isConnected) output += "MIDI connection available";
  else output += "Waiting for MIDI connection...";
  output += "\r\n";
  server.send(200, "text/plain", output);
}

// Upload file to SPIFFS. Maybe first look if file already exists and remove it?

void handleUpload() {
  static File fsUploadFile;
  HTTPUpload& upload = server.upload();
  if (upload.status == UPLOAD_FILE_START) {
    if (upload.filename.length() > 30) {
      upload.filename = upload.filename.substring(upload.filename.length() - 30, upload.filename.length());  // shorten filename
    }
    Serial.setDebugOutput(true);
    Serial.printf("FileUpload Name: /%s\n", upload.filename.c_str());
    fsUploadFile = SPIFFS.open("/" + server.urlDecode(upload.filename), "w");
  } else if (upload.status == UPLOAD_FILE_WRITE) {
    Serial.printf("FileUpload Data: %u\n", upload.currentSize);
    if (fsUploadFile)
      fsUploadFile.write(upload.buf, upload.currentSize);
  } else if (upload.status == UPLOAD_FILE_END) {
    if (fsUploadFile)
      fsUploadFile.close();
    Serial.printf("handleFileUpload Size: %u\n", upload.totalSize);
    server.sendContent("HTTP/1.1 303 OK\r\nLocation:/list\r\nCache-Control: no-cache\r\n");
  }
}

// This is all you need for OTA update!

void handleUpdate() {
  HTTPUpload& upload = server.upload();
  if (upload.status == UPLOAD_FILE_START) {
    Serial.setDebugOutput(true);
    Serial.printf("Update: %s\n", upload.filename.c_str());
    if (!Update.begin()) { //start with max available size
      Update.printError(Serial);
    }
  } else if (upload.status == UPLOAD_FILE_WRITE) {
    if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
      Update.printError(Serial);
    }
  } else if (upload.status == UPLOAD_FILE_END) {
    if (Update.end(true)) { //true to set the size to the current progress
      Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
    } else {
      Update.printError(Serial);
    }
    Serial.setDebugOutput(false);
  } else {
    Serial.printf("Update Failed Unexpectedly (likely broken connection): status=%d\n", upload.status);
  }
}

// Create HTML form to show preferences (and change them)

void handleConfig() {
  process(SPIFFS, "/config.html");
}

void handleTransp() {
  process(SPIFFS, "/transp.html");
}

// Change the preferences by POST request

void changeConfig() {
  if (server.hasArg("change") || server.hasArg("save")) {
    char pass1[32], pass2[32];
    if (server.hasArg("channelb")) channelb = server.arg("channelb").toInt();
    if (server.hasArg("channeld")) channeld = server.arg("channeld").toInt();
    if (server.hasArg("progb"))    progb    = server.arg("progb").toInt();
    if (server.hasArg("progd"))    progd    = server.arg("progd").toInt();
    if (server.hasArg("transp"))    transp   = server.arg("transp").toInt();
    octave = server.hasArg("octave");
    if (server.hasArg("volume"))   velocity = server.arg("volume").toInt();
    if (server.hasArg("ssid"))     server.arg("ssid").toCharArray(ssid, 30);
    if (server.hasArg("pass1"))    server.arg("pass1").toCharArray(pass1, 30);
    if (server.hasArg("pass2"))    server.arg("pass2").toCharArray(pass2, 30);
    if (strcmp(pass1, pass2) != 0) {
      server.send(200, "text/plain", "The passwords do not match - not changed!\r\n");
      return;
    }
    strcpy(pass, pass1);
    if (server.hasArg("save"))     putPreferences();
    printPreferences();
    setProgram(progb, channelb);
    if (channelb != channeld) setProgram(progd, channeld);

  }
  server.sendContent("HTTP/1.1 303 OK\r\nLocation:/config\r\nCache-Control: no-cache\r\n");
}

void changeTransp() {
  if (server.hasArg("up")) transp+=1;
  else if (server.hasArg("down")) transp+=11;
  transp %= 12;
  server.sendContent("HTTP/1.1 303 OK\r\nLocation:/transp\r\nCache-Control: no-cache\r\n");
}

// Insert current preferences into HTML page, using placeholders. Inspired by functions of the asyncWebServer.

void process(FS fs, char* filename) {
  // Serial.print("Processig file ");
  // Serial.println(filename);
  File file = fs.open(filename, "r");
  char HTML[4096] = "";
  char buf[32];
  int i = 0;
  while(file.available()) {
    char c = file.read();
    if (c != '%') {
      HTML[i++]= c;
     // Serial.print(c);
    } else {
      String name = "";
      while(file.available()) {
        char c = file.read();
        if (c != '%') name += c;
        else {
          buf[0] = 0;
          if (name == "CHANNELB")      sprintf(buf, "%d", channelb);
          else if (name == "CHANNELD") sprintf(buf, "%d", channeld);
          else if (name == "PROGB")    sprintf(buf, "%d", progb);
          else if (name == "PROGD")    sprintf(buf, "%d", progd);
          else if (name == "TRANSP")   sprintf(buf, "%d", transp);
          else if (name == "DURBASE")  sprintf(buf, "%s", durbase[transp]);
          else if (name == "VOLUME")   sprintf(buf, "%d", velocity);
          else if (name == "OCTAVE" && octave)   sprintf(buf, "%s", "checked");
          else if (name == "SSID")     sprintf(buf, "%s", ssid);
          else if (name == "PASS")     sprintf(buf, "%s", pass);
          strcat(HTML,buf);
          i = strlen(HTML);
          break;
        }
      }
    }
  }
  file.close();
  //Serial.println(HTML);
  server.send(200, "text/html", HTML);
}
