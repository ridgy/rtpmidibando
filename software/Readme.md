# Software

The software is written to be used with the Arduino IDE. You need to clone this whole folder.
This software only works for bandoneons with maximum 64 buttons (128 tones), as it is written for 4xMCP23017. For bandoneons with more tones, parts have to be rewritten.

### Bandonion.ino

This is the main program. It only works for ESP32 developer boards without modification. Just look at the comments in this file to understand how it works.

### data

You need to upload the files in this folder to the SPIFFS of the ESP32. They are used for:

- notes:    This file maps the switches to the MIDI pitches, so you can easily adopt to your needs
- index.html:   A rudimentary web server home page, to upload files to SPIFFS, reboot, and OTA updates
- config.html:  A rudimentary HTML form to display, change and save the preferences
- transp.html:  A minimal HTML form to simplify transposition
- style.css:    A rudimentary style file. Could be heavily improved in the future

If modifying index.html and/or config.html, the handler functions in the main program have also to be modified/extended.

### ToDo

Lots.

- clean up the code, add more documentation
- improve the functions presToVol() and presToBend() to simulate the behaviour of a real bandoneon
- improve and extend the functions of the webserver
- attach a list of links to the libraries/examples/documentations used to complete this project
- add more callback functions as handlers for incoming MIDI messages. As setting parameters via Web interface is very slow, this could be a possible mean to change settings (program, base volume, transposition etc.) in realtime
