# rtpmidibando

Modifying an old bandoneon to send midi notes/controls via rtpmidi

## Introduction

Years ago, I got me a bandoneon at ebay, for about 100 EUR. It was mechanically OK, the bellow was good, and after some work it also looked really good. But the reeds were corroded, and it was not worth replacing or repairing them. So, it laid around for years, and I thought of spending ist as decoration for a restaurant or music bar.

As I have been involved in some arduino projects throughout the last years, I thought of a new project to make an "electronic bandoneon" out of the old one, with which I also could play silently.

Removing the reed blocks gave access to the valves, and after some consideration I decided to use micro switches to detect open valves. Next, to read the pressure and direction, I looked for a suitable pressure sensor and found the SDP810/811, which is a two-way symmetric differential pressure sensor with I²C interface. So tests could start with some switches and MCP23017 I/O expanders to simulate the buttons and breath to simulate the pressure, and first idea was to use a simple Arduino nano as MCU. As the bandoneon should work wireless, first approach was two RFnano (= Arduino nano with builtin NRF24L wireless), one as sender, one as receiver, to connect via USB to the synthesizer (USB-MIDI). As a protocol raw MIDI seemed to be the easiest way.

When struggling with different MIDI libraries for arduino, I found there were some already standardized wireless MIDI protocols, so I switched to an ESP32 with WiFi and Bluetooth. First, I choose BLE-MIDI by simply building the packets myself and sending via the ESP32 BLE library. This worked fine, but the latency was not good. The BLE implementation of the ESP32 doesn't allow for latencies less than, say, 20ms.

As I could not get ipMidi running, I ended up with RTPMIDI (AppleMIDI). This is straightforward, well documented, the libraries and code examples are available (https://github.com/lathoub/Arduino-AppleMIDI-Library), and support for most platforms does exist. And the WiFi connection does allow latencies at about 2ms when not slowed down by routers or access points.

Using a WiFi solution, the webserver is not far away, as well as OTA updates (especially, when there is no USB connection to the outside). SPIFFS on the other hand makes it easy to configure the webserver and to save configuration files. So this was the basic configuration to start with.

Talking to some friends about the project, especially the mapping of buttons to pitches, the idea was born to enable a method of transposition, so the bandoneon could be tuned in any key.

## How it is made

### Hardware
 ![](hardware/Bando.jpg "Bass side before modification")

- An old 110 tone Bandoneon with the reed blocks removed
- 55 micro limit switches
- 1 sensirion SDP811 symmetric +-500Pa differential pressure sensor
- 4 MCP23017 breakout boards
- 1 ESP32 Developer Board
- 1 5000mAh LiPo battery
- 1 HW357 LiPo battery charge/discharge module
- 1 plug to connect external power (DC 4.5-8V)
- 1 small switch for power on/off
- a short hose to connect one side of the SDP to the outside
- cables, glue, solder, a test board, half of a micro USB cable, resistors, LEDs, ...

You can find the schematics and some pictures in the hardware folder. The schematic is very simple: 5V power supply, 3.3V VCC, GND, SCL and SDA - that's all!

### Software

See the software folder.

## How to use it

### Connecting to synthesizers

As the only OS I'm currently working with is Linux, I can't tell much about using other systems. AppleMIDI should work with MacOS and IOS ootb, and there are implementations for Windows as well.
Independent of the OS, there are two ways to connect to WiFi: Set the SSID and password in the configuration to an existing AP, or let the bandoneon create an AP and connect to that from your computer.

In the following scenario I will explain for a laptop equipped with Ubuntu Studio 18.04

You may create a WiFi access point on your computer with installing and configuring `hostapd`. This is the best way if you want to use your computer as MIDI synth and have more than one instrument. Search for `hostapd` for instructions how to do that.
Or, just let the bandoneon create the access point, and connect to that from your computer.

When switched on, the yellow LED will flash until there is a connection to a respective AP, or (after some seconds) the AP is started. Then, the yellow LED will lit.

Anyway, let's suppose your computer and the bandoneon have a WiFi connection, maybe even with an intermediate AP or router (but then be aware of the latency!).

First you need to install `rtpmidid`. You may install a commercial version from https://mclarenlabs.com/rtpmidi/, with GUI and some other features. I just used the minimal version from https://github.com/davidmoreno/rtpmidid. On this page you will also find hints for other OS/implementations and information on how to install.

When I started this project, the early versions of `rtpmidid` and the `AppleMIDI` library were different, and some things did not work. Using the latest versions now does work, but you may have to modify the configuration of `AppleMIDI` in file 'AppleMIDI_Settings.h', especially 'MaxSessionNameLen', as `rtpmidid` might send very long session names.

As soon as `rtpmidid` is installed and running, there should be an additional ALSA MIDI device. You may find out with

```
$ aconnect -l
client 0: 'System' [type=Kernel]
    0 'Timer           '
    1 'Announce        '
client 14: 'Midi Through' [type=Kernel]
    0 'Midi Through Port-0'
client 128: 'rtpmidi desk' [type=User,pid=9803]
    0 'Network         '
```
where client 128:0 is the writeable rtpmidi channel (all your values may differ; just look for the `rtpmidi` line). After powering on the bandoneon, this should read

```
client 128: 'rtpmidi desk' [type=User,pid=9803]
    0 'Network         '
    1 'bandonion       '
```

So now there is a readable MIDI channel you can connect to your synth software. Now let's start one, e.g. Qsynth, which is a frontend to fluidsynth (for more instructions, see the respective help pages and documentations elsewhere).
Now we have

```
client 128: 'rtpmidi desk' [type=User,pid=9803]
    0 'Network         '
    1 'bandonion       '
client 129: 'FLUID Synth (qsynth)' [type=User,pid=11491]
    0 'Synth input port (qsynth:0)'
```

With `aconnect 128:1 129:0` we connect the bandoneon to the synthesizer, which triggers the events:
- the bandoneon sends the program initialization to the synth, ends the `setup()` part and starts the `loop()`
- the yellow LED is switched off, and the green LED is lit
- the MIDI commands coming from the bandoneon will be interpreted by qsynth/fluidsynth and, if configured corectly, produce sound

Now have fun! Additionally, you may use `jack` and `qjackctl` to connect the instrument to DAWs and synth, but this is beyond this documentation.


### Using the web interface

As the hardware implementation has no external USB connector, there is a minimal web interface to modify SPIFFS files, configuration, and update the firmware.

Any time the bandoneon is connected to the network, you can call the web page through `http://bandonion.local` (supposed your computer supports mDNS). This gives the following start page:

![](mainPage.png)

Here you may request a minimal listing of the SPIFFS, upload new files or replace existing ones (important, if the `notes` file is not correct or you made changes to the HTML files), reboot the bandoneon, update the firmware or list/modify the configuration.
To update the firmware you need to save the binary e.g using the Arduino IDE (look for OTA documentation). Be sure to use the right one - otherwise you will have to unmount the hardware to get access to the USB port!
If you select to modify the configuration another page will be displayed:

![](configPage.png)

Here you may:
- change the MIDI channel numbers for the bass and discant side,
- change the MIDI program numbers respectively,
- transpose the tuning of the instrument in halftones from the basic "A" to some other key
- switch on octave chorus,
- change the base volume (the one sent with each "note on" command)
- modify the WiFi SSID and password

Modifying WiFi parameters will change the AP the bandoneon will connect to, as well as the settings of the bandoneon AP. Be careful setting the password; if you forget, you will have to unmount the hardware...
If you select "change", the values will be changed temporary (reboot will reset the values). Selecting "save" will change the parameters permanently.

There is also a "hidden" page `http://bandonion.local/transp` to simplify transposition (UP/DOWN halftones):

![](transpPage.png)

This still is very slow; to change some settings in realtime, a special (software) controller and some more MIDI callback functions would be nice. A first prototype of a callback is implemented as "OnMidiControlChange()". Ignoring the given channel, CC messages may be received. Here we use CC 3 (undefined) to set the transposition value. Using e.g. [SendMIDI](https://github.com/gbevin/SendMIDI#sendmidi), there is a fast method to transpose. A small script shows how to use:

```
#!/bin/bash
#
#  Send transposition messages to bandonion
#
(while read n; do
	echo "cc 3 $n"
done) | sendmidi dev bandonion --

```

After calling the script, just enter a number on the keyboard and hit "return". This will immediately change the transposition value.


**Edit:**

You may find some small scripts and programs in the `scripts` folder, to ease the connection and send control changes. Just look there. 
