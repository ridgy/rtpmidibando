#!/bin/bash
#
# Start all relevant programs and connections for bandoneon playing
#
#	Synth: qsynth
#	Connection: rtpmidid
#	
# First, start rtpmidid

/usr/bin/rtpmidid  &

# Second, start qsynth

/usr/bin/qsynth &

# Now try to connect the bandonion

sleep 5

/usr/bin/aconnect -l | \
	( while read line; do
	  if [[ $line == *FLUID\ Synth* ]] ; then export outc=$(echo $line | cut -d ' ' -f 2); fi
	  if [[ $line == *Synth\ input\ port* ]] ; then export outp=$(echo $line | cut -d ' ' -f 1); fi
	  if [[ $line == *rtpmidi* ]] ; then export inc=$(echo $line | cut -d ' ' -f 2); fi
	  if [[ $line == *bandonion* ]] ; then export inp=$(echo $line | cut -d ' ' -f 1); fi
    done
    if [ "$outc" -a "$outp" -a "$inc" -a "$inp" ] ; then 
      aconnect $inc$inp $outc$outp
    fi
  )
  
# wait for quitting, then kill rtpmidid

while read a ; do 
	if [ "$a" == "l" ] ; then aconnect -l; fi
	if [ "$a" == "q" ] ; then killall rtpmidid; exit 0; fi
done
