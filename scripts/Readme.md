## Helper scripts for Linux (tested with Ubuntu Studio 18.04)

**startall.sh**: Starts `rtpmidid` and `qsynth` and connects the respective sequential ports using `aconnect`. Expects the executables in /usr/bin. May be finsihed with "q", which will also kill rtpmidid.

**transp.sh**: Starts a local program derived from `xloadimage` to display a transposition and preset table

![](Transp.png)

and passes values calculated from button presses to local `sendmidi`. This is faster than the web interface, but still needs one hand to move the mouse and click the button. Best solution would be a series of foot switches to modify transposition and select program presets. The meaning of the presets is defined in Bandonion.ino.

Still has to be futher improved and documented.